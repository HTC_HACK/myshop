package uz.pdp.CRUD;

import com.google.gson.Gson;
import uz.pdp.Service.CrudRepository;
import uz.pdp.enums.Size;
import uz.pdp.model.Cloth;
import uz.pdp.model.Color;
import uz.pdp.model.Store;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;

import static uz.pdp.DataBase.*;
import static uz.pdp.DataBase.scannerInt;
import static uz.pdp.DataBase.scannerStr;
import static uz.pdp.Service.SessionMessage.*;
import static uz.pdp.Service.Util.*;

public class ClothCrud implements CrudRepository<Cloth> {
    @Override
    public void create() {
        System.out.println("Add or create: ");
        String option = scannerStr.nextLine();
        if (option.equalsIgnoreCase("create")) {
            if (!colorList.isEmpty()) {
                System.out.println("Enter name: ");
                String name = scannerStr.nextLine();
                size();
                System.out.println("Input size: ");
                String size = scannerStr.nextLine();
                Size newSize = Size.valueOf(size.toUpperCase());
                System.out.println(colorList);
                System.out.println("Input color: ");
                String color = scannerStr.nextLine();
                Color InputColor = null;
                for (Color color1 : colorList) {
                    if (color1.getName().equalsIgnoreCase(color)) {
                        InputColor = color1;
                    }
                }
                System.out.println("Input price: ");
                double price = scannerDou.nextDouble();
                System.out.println("Input discount: ");
                double discount = scannerDou.nextDouble();
                Cloth cloth = new Cloth(name, InputColor, newSize, price, discount);
                clothList.add(cloth);
                Store store = new Store(cloth, 1);
                storeList.add(store);
                writeJStore();
                writeJson();
                print(CYAN + "New cloth successfully created!!");
            } else return;
        } else {
            System.out.println("Enter name: ");
            String name = scannerStr.nextLine();
            size();
            System.out.println("Input size: ");
            String size = scannerStr.nextLine();
            Size newSize = Size.valueOf(size.toUpperCase());
            System.out.println(colorList);
            System.out.println("Input color: ");
            String color = scannerStr.nextLine();
            boolean isHave = false;
            for (Store store : storeList) {
                if (store.getCloth().getName().equalsIgnoreCase(name) && store.getCloth().getSize() == newSize &&
                        store.getCloth().getColor().equals(color)) {
                    System.out.println("Input quantity: ");
                    int quantity = scannerInt.nextInt();
                    store.setQuantity(store.getQuantity() + quantity);
                    print(GREEN + "New quantity successfully added!!");
                    isHave = true;
                    break;
                }
            }
            if (!isHave) print(RED + NOT_FOUND);
        }
    }

    @Override
    public void read() {
        for (Cloth cloth : clothList) {
            print(GREEN, "NAME : " + cloth.getName() + ", PRICE : " + cloth.getPrice() + ", Quantity: " + cloth.getQuantity());
            print(CYAN, "NEW PRICE : " + (cloth.getPrice() - (cloth.getPrice() * cloth.getDiscount() / 100)));
        }
    }

    @Override
    public void update() {
        read();
        System.out.println("Enter name: ");
        String name = scannerStr.nextLine();
        size();
        System.out.println("Input size: ");
        String size = scannerStr.nextLine();
        Size newSize = Size.valueOf(size.toUpperCase());
        System.out.println(colorList);
        System.out.println("Input color: ");
        String color = scannerStr.nextLine();
        Store selectedStoreItem = null;
        for (Store store : storeList) {
            if (store.getCloth().getName().equalsIgnoreCase(name) && store.getCloth().getSize() == newSize &&
                    store.getCloth().getColor().equals(color)) {
                selectedStoreItem = store;
                break;
            }
        }
        System.out.println("Input new name: ");
        String name1 = scannerStr.nextLine();
        selectedStoreItem.getCloth().setName(name1);
        size();
        System.out.println("Input new size: ");
        String size1 = scannerStr.nextLine();
        Size newSize1 = Size.valueOf(size.toUpperCase());
        selectedStoreItem.getCloth().setSize(newSize1);
        System.out.println(colorList);
        System.out.println("Input new color: ");
        String color1 = scannerStr.nextLine();
        Color InputColor = null;
        for (Color color2 : colorList) {
            if (color2.getName().equalsIgnoreCase(color1)) {
                InputColor = color2;
                break;
            }
        }
        selectedStoreItem.getCloth().setColor(InputColor);
        System.out.println("Input new price: ");
        double price = scannerDou.nextDouble();
        selectedStoreItem.getCloth().setPrice(price);
        System.out.println("Input new discount: ");
        double discount = scannerDou.nextDouble();
        selectedStoreItem.getCloth().setDiscount(discount);
        print(CYAN + "New cloth successfully created!!");
    }

    @Override
    public void delete() {
        try {
            if (clothList.remove(findById())) {
                print(RED, DELETED);
                writeJson();
            } else print(RED_BACKGROUND, ERROR);
        } catch (NullPointerException e) {
            print(CYAN, NOT_FOUND);
        }
    }

    @Override
    public Cloth findById() {
        read();
        print(CYAN, "Enter cloth name");
        String cloth = inputStr();
        return filter(cloth);
    }

    @Override
    public Cloth filter(String cloth) {
        return clothList.stream().filter(clothName -> clothName.getName().equalsIgnoreCase(cloth)).findFirst().orElse(null);
    }

    @Override
    public void writeJson() {
        try (Writer writer = new FileWriter("D:\\e-commerce\\ECommerce\\src\\main\\resources\\cloth.json")) {
            Gson gson = new Gson();
            String s = gson.toJson(clothList);
            writer.write(s);
            System.out.println("WROTE");
        } catch (IOException e) {
            System.out.println("NOT FOUND");
        }
    }

    public void writeJStore() {
        try (Writer writer = new FileWriter("D:\\e-commerce\\ECommerce\\src\\main\\resources\\store.json")) {
            Gson gson = new Gson();
            String s = gson.toJson(storeList);
            writer.write(s);
            System.out.println("WROTE");
        } catch (IOException e) {
            System.out.println("NOT FOUND");
        }
    }

    @Override
    public void crudMenu() {
        print(GREEN + "1=>Add cloth. 2=>Edit cloth. 3=>Remove cloth. 4=>Cloth list. 0=>BACK");
        int option = inputInt();
        switch (option) {
            case 1:
                create();
                break;
            case 2:
                update();
                break;
            case 3:
                delete();
                break;
            case 4:
                read();
                break;
            case 0:
                return;
        }
        crudMenu();
    }

    public void size() {
        Arrays.stream(Size.values()).forEach(s -> print(CYAN, String.valueOf(s)));
    }
}
