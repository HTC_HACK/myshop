package uz.pdp;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import uz.pdp.CRUD.ClothCrud;
import uz.pdp.CRUD.ColorCrud;
import uz.pdp.CRUD.PaymentCrud;
import uz.pdp.enums.Role;
import uz.pdp.model.*;
import uz.pdp.abs.User;
import uz.pdp.enums.Size;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static uz.pdp.Service.SessionMessage.*;
import static uz.pdp.Service.Util.*;

public class DataBase {
    public static ClothCrud clothCrud = new ClothCrud();
    public static ColorCrud colorCrud = new ColorCrud();
    public static PaymentCrud paymentCrud = new PaymentCrud();

    public static Scanner scannerInt = new Scanner(System.in);
    public static Scanner scannerStr = new Scanner(System.in);
    public static Scanner scannerDou = new Scanner(System.in);
    public static List<User> userList = new ArrayList<>(Arrays.asList(
            new Admin("admin", "a", "1", 0),
            new Customer("Asadbek Halimjonov", "b", "1", 1_000_000)
    ));


    public static List<Color> colorList = new ArrayList<>(Arrays.asList(
            new Color("Black"),
            new Color("Blue"),
            new Color("Dark blue"),
            new Color("Red")
    ));

    public static List<Cloth> clothList = new ArrayList<>(Arrays.asList(
            new Cloth("T-Shirt", colorList.get(0), Size.L, 30_000, 0),
            new Cloth("T-Shirt (red)", colorList.get(3), Size.L, 60_000, 5),
            new Cloth("Cap", colorList.get(2), Size.XL, 10000, 0)
    ));


    public static List<Store> storeList = new ArrayList<>(Arrays.asList(
            new Store(clothList.get(0), 15),
            new Store(clothList.get(1), 4),
            new Store(clothList.get(2), 10)
    ));


    public static List<PayType> payTypeList = new ArrayList<>(Arrays.asList(
            new PayType("Payme", 1),
            new PayType("Click", 0.5)
    ));

    public static List<OrderHistory> orderHistoryList = new ArrayList<>();

    public static List<OrderItem> orderItemList = new ArrayList<>();
    public static List<OrderItem> tempOrderItemList = new ArrayList<>();
    public static List<Cloth> myCartList = new ArrayList<>();

    public void migration() {
        System.out.println("RUNNING DATABASE");

        File payment = new File("D:\\e-commerce\\ECommerce\\src\\main\\resources\\payment.json");
        // TODO: 12/6/21 done
        if (payment.length() == 0) {
            paymentCrud.writeJson();
        } else {
            try (Reader reader = new FileReader(payment)) {
                Gson gson = new Gson();
                List<PayType> payTypes = gson.fromJson(reader, new TypeToken<List<PayType>>() {
                }.getType());
                payTypeList.removeAll(payTypeList);
                payTypeList.addAll(payTypes);

                print(CYAN, "PAYMENT .. " + STATUS + "..." + SUCCESS);
            } catch (IOException e) {
                print(RED, "PAYMENT .. " + STATUS + "..." + FAILED);
            }
        }

        File color = new File("D:\\e-commerce\\ECommerce\\src\\main\\resources\\color.json");
        if (color.length() == 0) {
            colorCrud.writeJson();
        } else {
            try (Reader reader = new FileReader(color)) {
                Gson gson = new Gson();
                List<Color> colorTypes = gson.fromJson(reader, new TypeToken<List<Color>>() {
                }.getType());
                colorList.removeAll(colorList);
                colorList.addAll(colorTypes);
                print(CYAN, "COLOR .. " + STATUS + "..." + SUCCESS);
            } catch (IOException e) {
                print(RED, "COLOR .. " + STATUS + "..." + FAILED);
            }
        }

        File clothData = new File("D:\\e-commerce\\ECommerce\\src\\main\\resources\\cloth.json");
        // TODO: 12/6/21 done
        if (clothData.length() == 0) {
            clothCrud.writeJson();
        } else {
            try (Reader reader = new FileReader(clothData)) {
                Gson gson = new Gson();
                List<Cloth> clothTypes = gson.fromJson(reader, new TypeToken<List<Cloth>>() {
                }.getType());
                clothList.removeAll(clothList);
                clothList.addAll(clothTypes);
                print(CYAN, "CLOTH .. " + STATUS + "..." + SUCCESS);
            } catch (IOException e) {
                print(RED, "CLOTH .. " + STATUS + "..." + FAILED);
            }
        }

        File storeData = new File("D:\\e-commerce\\ECommerce\\src\\main\\resources\\store.json");
        // TODO: 12/6/21 done
        if (storeData.length() == 0) {
            clothCrud.writeJStore();
        } else {
            try (Reader reader = new FileReader(clothData)) {
                Gson gson = new Gson();
                List<Store> storeTypes = gson.fromJson(reader, new TypeToken<List<Store>>() {
                }.getType());
                storeList.removeAll(storeList);
                storeList.addAll(storeTypes);
                print(CYAN, "STACK .. " + STATUS + "..." + SUCCESS);
            } catch (IOException e) {
                print(RED, "STACK .. " + STATUS + "..." + FAILED);
            }
        }

    }
}
