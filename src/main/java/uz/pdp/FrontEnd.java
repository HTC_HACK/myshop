package uz.pdp;

import uz.pdp.Service.*;
import uz.pdp.ServiceImpl.AuthServiceImpl;
import uz.pdp.ServiceImpl.OrderServiceImpl;

import static uz.pdp.Service.Util.CYAN;
import static uz.pdp.Service.Util.print;

public class FrontEnd {
    static AuthServiceImpl authService = new AuthServiceImpl();
    static OrderServiceImpl orderService = new OrderServiceImpl();
    public void MainMenu() {
        print(CYAN, "1=>CLOTHE LIST. 2=>MY CART. 3=>LOGIN. 4=>REGISTER. 0=>EXIT");
        int option = Util.inputInt();
        switch (option) {
            case 1:
                orderService.addToCart(null);
                break;
            case 2:
                orderService.myCart(null);
                break;
            case 3:
                authService.login();
                break;
            case 4:
                authService.register();
                break;
            case 0:
                return;
        }
        MainMenu();
    }
}
