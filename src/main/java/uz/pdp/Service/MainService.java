package uz.pdp.Service;

import uz.pdp.abs.User;
import uz.pdp.model.Customer;

public interface MainService {
    void showMenu(User user);

    void seeClothes(User user);

    void myCart(Customer customer);
}
