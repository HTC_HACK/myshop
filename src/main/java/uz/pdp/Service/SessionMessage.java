package uz.pdp.Service;

public interface SessionMessage {
    String SUCCESS = "SUCCESS";
    String CREATED = "CREATED";
    String UPDATED = "UPDATED";
    String DELETED = "DELETED";
    String ERROR = "ERROR";
    String WRONG_OPTION = "WRONG OPTION";
    String NOT_FOUND = "NOT_FOUND";
    String ALREADY_EXIST = "ALREADY_EXIST";
    String STATUS = "STATUS";
    String FAILED = "FAILED";

    String LOGIN_SUCCESS = "SUCCESSFULLY LOGIN";
    String LOGOUT_SUCCESS = "SUCCESSFULLY LOGOUT";

    String WRITED = "WRITED";


}
