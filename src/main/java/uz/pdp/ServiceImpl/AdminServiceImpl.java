package uz.pdp.ServiceImpl;

import com.google.gson.Gson;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import uz.pdp.Service.AdminService;
import uz.pdp.ServiceImpl.AuthServiceImpl;
import uz.pdp.model.Cloth;

import java.io.*;
import java.util.Arrays;

import static uz.pdp.DataBase.*;
import static uz.pdp.Service.Util.*;


public class AdminServiceImpl implements AdminService {
    static AuthServiceImpl authService = new AuthServiceImpl();
    static MainServiceImpl mainService = new MainServiceImpl();
    @Override
    public void adminMenu() {
        print(CYAN, "1=>User Action. 2=>Cloth Action. 3=>Pay type Action. 4=> Order histories. 5=> Show store items. 6=>Color Action 0=>Logout");
        int optionAdmin = inputInt();
        switch (optionAdmin) {
            case 1:
                authService.showUsers();
                break;
            case 2:
                clothCrud.crudMenu();
                break;
            case 3:
                paymentCrud.crudMenu();
                break;
            case 4:
                mainService.showOrderList(null);
                break;
            case 5:
                showStoreItem();
                break;
            case 6:
                colorCrud.crudMenu();
                break;
            case 0:
                return;
        }

        adminMenu();
    }

    private void showStoreItem() {
                try (FileOutputStream fileOutputStream = new FileOutputStream("src/main/resources/store.xlsx")) {
                    XSSFWorkbook workbook = new XSSFWorkbook();
                    XSSFSheet sheet = workbook.createSheet();
                    sheet.setDefaultColumnWidth(20);
                    sheet.setDefaultRowHeightInPoints(50);
                    XSSFRow row = sheet.createRow(0);
                    CellStyle cellStyle = workbook.createCellStyle();
                    cellStyle.setAlignment(HorizontalAlignment.CENTER);
                    cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                    int priceColNum = 3;
                    row.createCell(0).setCellValue("T/R");
                    row.getCell(0).setCellStyle(cellStyle);
                    row.createCell(1).setCellValue("Name");
                    row.getCell(1).setCellStyle(cellStyle);
                    row.createCell(2).setCellValue("Size");
                    row.getCell(2).setCellStyle(cellStyle);
                    row.createCell(priceColNum).setCellValue("Price");
                    row.getCell(priceColNum).setCellStyle(cellStyle);
                    try (Reader reader = new FileReader("src/main/resources/store.json")) {
                        Gson gson = new Gson();
                        Cloth[] list = gson.fromJson(reader, Cloth[].class);
                        System.out.println(Arrays.toString(list));
                        for (int i = 0; i < list.length; i++) {
                            Cloth cloth = list[i];
                            XSSFRow newRow = sheet.createRow(i + 1);
                            newRow.createCell(0).setCellValue(i + 1);
                            newRow.getCell(0).setCellStyle(cellStyle);
                            newRow.createCell(1).setCellValue(cloth.getName());
                            newRow.getCell(1).setCellStyle(cellStyle);
                            newRow.createCell(2).setCellValue(String.valueOf(cloth.getSize()));
                            newRow.getCell(2).setCellStyle(cellStyle);
                            newRow.createCell(priceColNum).setCellValue(cloth.getPrice());
                            newRow.getCell(priceColNum).setCellStyle(cellStyle);
                        }
                        int totalSumRowNum = list.length + 1;
                        XSSFRow totalSumRow = sheet.createRow(totalSumRowNum);
                        XSSFCell cell = totalSumRow.createCell(priceColNum);
                        XSSFCell totalSumStrCell = totalSumRow.createCell(priceColNum - 1);
                        totalSumStrCell.setCellValue("Total sum: ");
                        totalSumStrCell.setCellStyle(cellStyle);
                        totalSumRow.getCell(priceColNum).setCellStyle(cellStyle);
                        String column_letter = CellReference.convertNumToColString(priceColNum);
                        String endRow = column_letter + totalSumRowNum;
                        cell.setCellFormula("SUM(" + column_letter + "2:" + endRow + ")");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    workbook.write(fileOutputStream);
                    System.out.println("Successfully created!");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
    }
}
