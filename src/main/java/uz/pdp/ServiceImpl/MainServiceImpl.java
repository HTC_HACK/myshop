package uz.pdp.ServiceImpl;


import com.google.gson.Gson;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import uz.pdp.Service.MainService;
import uz.pdp.Service.Util;
import uz.pdp.abs.User;
import uz.pdp.model.OrderHistory;
import uz.pdp.model.*;

import java.awt.*;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static uz.pdp.DataBase.*;
import static uz.pdp.Service.SessionMessage.*;
import static uz.pdp.Service.Util.scannerInt;
import static uz.pdp.Service.Util.scannerStr;
import static uz.pdp.Service.Util.*;


public class MainServiceImpl implements MainService {
    AuthServiceImpl authService = new AuthServiceImpl();

    @Override
    public void showMenu(User user) {
        while (true) {
            print(BLUE, "1=> See clothes, 2=> My cart, 3=> Order history,  0=> Back");
            int option = inputInt();

            switch (option) {
                case 1:
                    Util.printInRed("======= CLOTHES ======");
                    seeClothes(user);
                    break;
                case 2:
                    Util.printInRed("======= MY CART ======");
                    myCart((Customer) user);
                    break;
                case 3:
                    Util.printInRed("======= ORDER HISTORY ======");
                    showOrderList(user);
                    break;
                case 0:
                    return;
                default:
                    Util.printInRed("Wrong option");
                    break;
            }

        }
    }

    public void showOrderList(User user) {
        if (user != null)
        {
            for (OrderHistory orderHistory : orderHistoryList) {
                if (orderHistory.getCustomer().getUsername().equals(user.getUsername()))
                System.out.println(orderHistory);
            }
            try (FileOutputStream fileOutputStream = new FileOutputStream("src/main/resources/orderHistoryList.xlsx")) {
                XSSFWorkbook workbook = new XSSFWorkbook();
                XSSFSheet sheet = workbook.createSheet();
                sheet.setDefaultColumnWidth(20);
                sheet.setDefaultRowHeightInPoints(50);
                XSSFRow row = sheet.createRow(0);
                CellStyle cellStyle = workbook.createCellStyle();
                cellStyle.setAlignment(HorizontalAlignment.CENTER);
                cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                int priceColNum = 4;
                row.createCell(0).setCellValue("T/R");
                row.getCell(0).setCellStyle(cellStyle);
                row.createCell(1).setCellValue("Customer");
                row.getCell(1).setCellStyle(cellStyle);
                row.createCell(2).setCellValue("Cloth");
                row.getCell(2).setCellStyle(cellStyle);
                row.createCell(3).setCellValue("Pay type");
                row.getCell(3).setCellStyle(cellStyle);
                row.createCell(priceColNum).setCellValue("Price");
                row.getCell(priceColNum).setCellStyle(cellStyle);
                int sum = 0;
                try (Reader reader = new FileReader("src/main/resources/OrderHistory.json")) {
                    Gson gson = new Gson();
                    OrderHistory[] list = gson.fromJson(reader, OrderHistory[].class);
                    System.out.println(Arrays.toString(list));

                    for (int i = 0; i < list.length; i++) {
                        OrderHistory cloth = list[i];
                        if (cloth.getCustomer().getUsername().equals(user.getUsername())) {
                            XSSFRow newRow = sheet.createRow(i + 1);
                            newRow.createCell(0).setCellValue(i + 1);
                            newRow.getCell(0).setCellStyle(cellStyle);
                            newRow.createCell(1).setCellValue(cloth.getCustomer().getFullName());
                            newRow.getCell(1).setCellStyle(cellStyle);
                            newRow.createCell(2).setCellValue(cloth.getItems().get(i).getCloth().getName() + "/" + cloth.getItems().get(i).getCloth().getSize());
                            newRow.getCell(2).setCellStyle(cellStyle);
                            newRow.createCell(3).setCellValue(cloth.getPayType().getName());
                            newRow.getCell(3).setCellStyle(cellStyle);
                            newRow.createCell(priceColNum).setCellValue("" + cloth.getPrice());
                            newRow.getCell(priceColNum).setCellStyle(cellStyle);
                            sum += cloth.getPrice();
                        }
                    }
                    row = sheet.createRow(list.length + 1);
                    row.createCell(0).setCellValue("");
                    row.getCell(0).setCellStyle(cellStyle);
                    row.createCell(1).setCellValue("");
                    row.getCell(1).setCellStyle(cellStyle);
                    row.createCell(2).setCellValue("");
                    row.getCell(2).setCellStyle(cellStyle);
                    row.createCell(3).setCellValue("Total");
                    row.getCell(3).setCellStyle(cellStyle);
                    row.createCell(priceColNum).setCellValue("" + sum);
                    row.getCell(priceColNum).setCellStyle(cellStyle);


                } catch (IOException e) {
                    e.printStackTrace();
                }
                workbook.write(fileOutputStream);
                System.out.println("Successfully created! Just open *orderHistoryList.xlsx* file!!");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            for (OrderHistory orderHistory : orderHistoryList) {
                System.out.println(orderHistory);
            }
            try (FileOutputStream fileOutputStream = new FileOutputStream("src/main/resources/orderHistoryList.xlsx")) {
                XSSFWorkbook workbook = new XSSFWorkbook();
                XSSFSheet sheet = workbook.createSheet();
                sheet.setDefaultColumnWidth(20);
                sheet.setDefaultRowHeightInPoints(50);
                XSSFRow row = sheet.createRow(0);
                CellStyle cellStyle = workbook.createCellStyle();
                cellStyle.setAlignment(HorizontalAlignment.CENTER);
                cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                int priceColNum = 4;
                row.createCell(0).setCellValue("T/R");
                row.getCell(0).setCellStyle(cellStyle);
                row.createCell(1).setCellValue("Customer");
                row.getCell(1).setCellStyle(cellStyle);
                row.createCell(2).setCellValue("Cloth");
                row.getCell(2).setCellStyle(cellStyle);
                row.createCell(3).setCellValue("Pay type");
                row.getCell(3).setCellStyle(cellStyle);
                row.createCell(priceColNum).setCellValue("Price");
                row.getCell(priceColNum).setCellStyle(cellStyle);
                int sum = 0;
                try (Reader reader = new FileReader("src/main/resources/OrderHistory.json")) {
                    Gson gson = new Gson();
                    OrderHistory[] list = gson.fromJson(reader, OrderHistory[].class);
                    System.out.println(Arrays.toString(list));
                    for (int i = 0; i < list.length; i++) {
                        OrderHistory cloth = list[i];
                        XSSFRow newRow = sheet.createRow(i + 1);
                        newRow.createCell(0).setCellValue(i + 1);
                        newRow.getCell(0).setCellStyle(cellStyle);
                        newRow.createCell(1).setCellValue(cloth.getCustomer().getFullName());
                        newRow.getCell(1).setCellStyle(cellStyle);
                        newRow.createCell(2).setCellValue(cloth.getItems().get(i).getCloth().getName() + "/" + cloth.getItems().get(i).getCloth().getSize());
                        newRow.getCell(2).setCellStyle(cellStyle);
                        newRow.createCell(3).setCellValue(cloth.getPayType().getName());
                        newRow.getCell(3).setCellStyle(cellStyle);
                        newRow.createCell(priceColNum).setCellValue("" + cloth.getPrice());
                        newRow.getCell(priceColNum).setCellStyle(cellStyle);
                        sum += cloth.getPrice();
                    }
                    row = sheet.createRow(list.length + 1);
                    row.createCell(0).setCellValue("");
                    row.getCell(0).setCellStyle(cellStyle);
                    row.createCell(1).setCellValue("");
                    row.getCell(1).setCellStyle(cellStyle);
                    row.createCell(2).setCellValue("");
                    row.getCell(2).setCellStyle(cellStyle);
                    row.createCell(3).setCellValue("Total");
                    row.getCell(3).setCellStyle(cellStyle);
                    row.createCell(priceColNum).setCellValue("" + sum);
                    row.getCell(priceColNum).setCellStyle(cellStyle);


                } catch (IOException e) {
                    e.printStackTrace();
                }
                workbook.write(fileOutputStream);
                System.out.println("Successfully created! Just open *orderHistoryList.xlsx* file!!");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void seeClothes(User user) {

        Cloth selectedCloth = clothCrud.findById();
        if (selectedCloth != null) {
            System.out.println("Enter quantity");
            int quantity = inputInt();
            if (user != null) {
                Customer customer = (Customer) user;
                OrderItem orderItem3 = customer.getMyCart().stream().filter(mycart -> mycart.getCloth().getName().equalsIgnoreCase(selectedCloth.getName())).findFirst().orElse(null);
                if (orderItem3 != null) {
                    orderItem3.setQuantity(orderItem3.getQuantity() + quantity);
                    print(CYAN, SUCCESS);
                    authService.writeJson();
                } else {
                    OrderItem orderItem = new OrderItem(selectedCloth, quantity);
                    customer.setMyCart(orderItem);
                    print(CYAN, SUCCESS);
                    authService.writeJson();
                }
            } else {
                OrderItem orderItem = new OrderItem(selectedCloth, quantity);
                tempOrderItemList.add(orderItem);
                print(CYAN, SUCCESS);
            }

        }
    }

    @Override
    public void myCart(Customer customer) {
        customer.getMyCart().stream().forEach(orderItem -> print(BLUE, "NAME : " + orderItem.getCloth().getName() + ", " + "Size : " + orderItem.getCloth().getSize() +
                " Color : " + orderItem.getCloth().getColor() + " QUANTITY : " + orderItem.getQuantity() + ", PRICE : " + orderItem.getCloth().getPrice()));
        System.out.println("Name: ");
        String name = scannerStr.nextLine();
//        System.out.println("Size: ");
//        Size size = Size.valueOf(scannerStr.nextLine().toUpperCase());
//        System.out.println("Color: ");
//        String color = scannerStr.nextLine().toUpperCase();
        System.out.println("Input quantity: ");
        int quantity = scannerInt.nextInt();
        System.out.println(payTypeList);
        System.out.println("Input pay type: ");
        String namePayType = scannerStr.nextLine();
        PayType selectedPayType = null;
        for (PayType payType : payTypeList) {
            if (namePayType.equalsIgnoreCase(payType.getName())) {
                selectedPayType = payType;
                break;
            }
        }
        for (OrderItem cloth : customer.getMyCart()) {
            if (cloth.getCloth().getName().equalsIgnoreCase(name)) {
                orderItemList.add(new OrderItem(cloth.getCloth(), quantity));
                orderHistoryList.add(new OrderHistory(customer, orderItemList, cloth.getCloth().getPrice() * quantity, selectedPayType.getCommissionFee(), selectedPayType));
                //getCheck(selectedPayType, cloth);
                System.out.println("Thank you for your choice!!!");
                print(RED, "DO YOU WANT CHECK YES=>1, NO=>2");
                int optionCheck = inputInt();
                switch (optionCheck) {
                    case 1:
                        try (PdfWriter writer = new PdfWriter("src/main/resources/folder/check.pdf")) {

                            PdfDocument pdfDocument = new PdfDocument(writer);

                            pdfDocument.setDefaultPageSize(PageSize.A4);
                            pdfDocument.addNewPage();


                            Document document = new Document(pdfDocument);
                            Paragraph paragraph = new Paragraph("CHECK " + customer.getUsername().toUpperCase() + " - DATE " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))).setFontSize(25);

                            document.add(paragraph);


                            float[] pointColumn = {60F, 120F, 120F, 120F, 120F};
                            Table table = new Table(pointColumn);

                            table.setTextAlignment(TextAlignment.CENTER).setHorizontalAlignment(com.itextpdf.layout.property.HorizontalAlignment.CENTER);
                            table.addCell(new Cell().add("ID").setBackgroundColor(com.itextpdf.kernel.color.Color.BLUE));
                            table.addCell(new Cell().add("NAME").setBackgroundColor(com.itextpdf.kernel.color.Color.BLUE));
                            table.addCell(new Cell().add("SIZE").setBackgroundColor(com.itextpdf.kernel.color.Color.BLUE));
                            table.addCell(new Cell().add("PRICE").setBackgroundColor(com.itextpdf.kernel.color.Color.BLUE));
                            table.addCell(new Cell().add("QUANTITY").setBackgroundColor(com.itextpdf.kernel.color.Color.BLUE));


                            int i = 1;
                            int sum = 0;
                            for (OrderItem orderItem : customer.getMyCart()) {
                                if (i % 2 == 0) {
                                    table.addCell(new Cell().add("" + (i++)).setBackgroundColor(com.itextpdf.kernel.color.Color.LIGHT_GRAY));
                                    table.addCell(new Cell().add(orderItem.getCloth().getName()).setBackgroundColor(com.itextpdf.kernel.color.Color.LIGHT_GRAY));
                                    table.addCell(new Cell().add(orderItem.getCloth().getSize().name()).setBackgroundColor(com.itextpdf.kernel.color.Color.LIGHT_GRAY));
                                    table.addCell(new Cell().add("" + orderItem.getCloth().getPrice()).setBackgroundColor(com.itextpdf.kernel.color.Color.LIGHT_GRAY));
                                    table.addCell(new Cell().add("" + orderItem.getQuantity()).setBackgroundColor(com.itextpdf.kernel.color.Color.LIGHT_GRAY));
                                } else {
                                    table.addCell(new Cell().add("" + (i++)).setBackgroundColor(com.itextpdf.kernel.color.Color.GRAY));
                                    table.addCell(new Cell().add(orderItem.getCloth().getName()).setBackgroundColor(com.itextpdf.kernel.color.Color.GRAY));
                                    table.addCell(new Cell().add(orderItem.getCloth().getSize().name()).setBackgroundColor(com.itextpdf.kernel.color.Color.GRAY));
                                    table.addCell(new Cell().add("" + orderItem.getCloth().getPrice()).setBackgroundColor(com.itextpdf.kernel.color.Color.GRAY));
                                    table.addCell(new Cell().add("" + orderItem.getQuantity()).setBackgroundColor(com.itextpdf.kernel.color.Color.GRAY));
                                }

                                sum += orderItem.getQuantity() * orderItem.getCloth().getPrice();
                            }

                            table.addCell(new Cell().add(""));
                            table.addCell(new Cell().add(""));
                            table.addCell(new Cell().add(""));
                            table.addCell(new Cell().add("TOTAL COST").setBackgroundColor(com.itextpdf.kernel.color.Color.RED));
                            table.addCell(new Cell().add("" + sum).setBackgroundColor(com.itextpdf.kernel.color.Color.RED));
                            table.addCell(new Cell().add(""));
                            table.addCell(new Cell().add(""));
                            table.addCell(new Cell().add(""));
                            table.addCell(new Cell().add("FEE : " + selectedPayType.getName()).setBackgroundColor(com.itextpdf.kernel.color.Color.LIGHT_GRAY));
                            table.addCell(new Cell().add("" + selectedPayType.getCommissionFee()).setBackgroundColor(com.itextpdf.kernel.color.Color.LIGHT_GRAY));
                            table.addCell(new Cell().add(""));
                            table.addCell(new Cell().add(""));
                            table.addCell(new Cell().add(""));
                            table.addCell(new Cell().add("TOTAL COST").setBackgroundColor(com.itextpdf.kernel.color.Color.RED));
                            table.addCell(new Cell().add("" + (sum + sum * selectedPayType.getCommissionFee() / 100)).setBackgroundColor(Color.RED));

                            document.add(table);

                            document.close();
                            Desktop d = Desktop.getDesktop();
                            d.open(new File("src/main/resources/folder/check.pdf"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 2:
                        break;
                }
                print(CYAN, SUCCESS);
                if (cloth.getQuantity() > quantity)
                    cloth.setQuantity(cloth.getQuantity() - quantity);
                else if (cloth.getQuantity() == quantity) myCartList.remove(cloth);
                else System.out.println("Not enough cloth quantity");
                System.out.println("Successfully purchased!!");
            }
        }
        customer.clear();

        writeJson();

    }

    public void writeJson() {
        try (Writer writer = new FileWriter("src/main/resources/OrderHistory.json")) {
            Gson gson = new Gson();
            String s = gson.toJson(orderHistoryList);
            writer.write(s);
            print(BLUE, WRITED);
        } catch (IOException e) {
            print(RED, NOT_FOUND);
        }
    }

}
