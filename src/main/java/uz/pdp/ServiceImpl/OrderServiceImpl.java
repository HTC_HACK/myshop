package uz.pdp.ServiceImpl;

import uz.pdp.Service.OrderService;
import uz.pdp.Service.Util;
import uz.pdp.abs.User;
import uz.pdp.model.Cloth;
import uz.pdp.model.Customer;
import uz.pdp.model.OrderItem;

import static uz.pdp.DataBase.*;
import static uz.pdp.Service.SessionMessage.SUCCESS;
import static uz.pdp.Service.Util.*;

public class OrderServiceImpl implements OrderService {
    static AuthServiceImpl authService = new AuthServiceImpl();
    static MainServiceImpl mainService = new MainServiceImpl();
    @Override
    public void addToCart(User user) {
        Cloth selectedCloth = clothCrud.findById();
        if (selectedCloth != null) {
            System.out.println("Enter quantity");
            int quantity = inputInt();
            if (user == null) {
                OrderItem orderItem = new OrderItem(selectedCloth, quantity);
                tempOrderItemList.add(orderItem);
                print(CYAN, SUCCESS);
            }
        }
    }

    @Override
    public void orderMenu(User user) {

    }

    @Override
    public void myCart(User user) {
        boolean active = true;
        while (active){
            print(RED, "1=>Check out. 2=> Order history. 0=>Back");
            int opt = Util.inputInt();
            switch (opt) {
                case 1:
                    System.out.println("1=> Login, 2=> Register");
                    int option = Util.inputInt();
                    switch (option) {
                        case 1:
                            loginWithoutReg();
                            break;
                        case 2:
                            authService.register();
                            break;
                    }
                    break;
                case 2:
                    mainService.showOrderList(user);
                    break;
                case 0:
                    active = false;
                    break;
            }
        }
    }

    private void loginWithoutReg() {
        print(CYAN, "Enter username");
        String username = Util.inputStr();

        print(CYAN, "Enter password");
        String password = Util.inputStr();

        User user = userList.stream().filter(user1 -> user1.getUsername().equals(username)
                && user1.getPassword().equals(password)).findFirst().orElse(null);

        for (OrderItem orderItem : tempOrderItemList) {
            System.out.println("Name: " + orderItem.getCloth().getName() + "\n" + "Size: " + orderItem.getCloth().getSize() +
                    "\nColor" + orderItem.getCloth().getColor().getName());
        }
        MainServiceImpl mainServiceImpl = new MainServiceImpl();
        mainServiceImpl.myCart((Customer) user);
        tempOrderItemList.removeAll(tempOrderItemList);
    }
}
