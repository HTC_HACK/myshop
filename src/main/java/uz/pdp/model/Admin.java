package uz.pdp.model;

import uz.pdp.abs.User;
import uz.pdp.enums.Role;

public class Admin extends User {

    public Admin(String fullName, String username, String password, double balance) {
        super(fullName, username, password, balance, Role.ADMIN);
    }
}
