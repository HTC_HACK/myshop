package uz.pdp.model;

import uz.pdp.abs.AbsClothQuantity;
import uz.pdp.enums.Size;

public class Cloth extends AbsClothQuantity {
    private String name;
    private int productCode = (int) (Math.random() * (9999 - 1000)) + 1000;
    private Color color;
    private Size size;
    private double price;
    private double discount;

    public Cloth() {
    }

    public Cloth(String name, Color color, Size size, double price, double discount) {
        this.name = name;
        this.color = color;
        this.size = size;
        this.price = price;
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
}
