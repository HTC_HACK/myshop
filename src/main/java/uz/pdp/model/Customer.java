package uz.pdp.model;

import uz.pdp.abs.User;
import uz.pdp.enums.Role;

import java.util.ArrayList;
import java.util.List;

public class Customer extends User {
    private boolean isActive = true;
    private List<OrderItem> myCart = new ArrayList<>();


    public Customer(String fullName, String username, String password, double balance) {
        super(fullName, username, password, balance, Role.CUSTOMER);
    }
    public void clear()
    {
        this.myCart.clear();
    }

    public void setMyCart(OrderItem myCart) {
        this.myCart.add(myCart);
    }

    public List<OrderItem> getMyCart() {
        return myCart;
    }
}
